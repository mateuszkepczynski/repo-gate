import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-08-31.
 */
public class GateServer {

    private Map<Integer,String> regIdGateMap;
    private IRate currentRate = Rate.RATENIGHT;
    private Map<Gate, Parking> gatesToParking = new HashMap<>();
    private Map<String, Parking> parkings = new HashMap<>();

    public Map<Integer, String> getRegIdGateMap() {
        return regIdGateMap;
    }

    public Map<Gate, Parking> getGatesToParking() {
        return gatesToParking;
    }

    public Ticket generateTicket(String nrRej) {
        Ticket ticket = new Ticket(currentRate.getRate());

        return ticket;

    }

}
