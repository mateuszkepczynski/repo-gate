/**
 * Created by RENT on 2017-08-31.
 */
public class Ticket {
    public static int counter = 0;
    private int id;
    private long timestampIn;
    private long timestampOut;
    private long timestampPay;
    private boolean paid;
    private double amountToPay;
    private double converterInfo;

    public Ticket(double converterInfo) {

        this.counter = counter++;
        this.id = id;
        this.timestampIn = System.currentTimeMillis();
        this.timestampOut = 0;
        this.timestampPay = 0;
        this.paid = false;
        this.amountToPay = 0;
        this.converterInfo = converterInfo;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Ticket.counter = counter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTimestampIn() {
        return timestampIn;
    }

    public void setTimestampIn(long timestampIn) {
        this.timestampIn = timestampIn;
    }

    public long getTimestampOut() {
        return timestampOut;
    }

    public void setTimestampOut(long timestampOut) {
        this.timestampOut = timestampOut;
    }

    public long getTimestampPay() {
        return timestampPay;
    }

    public void setTimestampPay(long timestampPay) {
        this.timestampPay = timestampPay;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public double getConverterInfo() {
        return converterInfo;
    }

    public void setConverterInfo(double converterInfo) {
        this.converterInfo = converterInfo;
    }

    public long getStayLength() {
        return System.currentTimeMillis() - timestampIn;
    }

}
