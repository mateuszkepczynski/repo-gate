import java.util.List;

/**
 * Created by RENT on 2017-08-31.
 */
public class Parking {
    private int space;
    private List<Ticket> ticketList;
    private String name;

    public Parking(int space, List<Ticket> ticketList, String name) {
        this.space = space;
        this.ticketList = ticketList;
        this.name = name;
    }

    public boolean hasFreeSpot (){
        return space>ticketList.size();
    }

}
