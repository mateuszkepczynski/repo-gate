import java.util.Optional;

/**
 * Created by RENT on 2017-08-31.
 */
public class Gate {
    public static int counter = 0;
    private int gateId;
    private boolean isOpen;
    private GateServer gateServer;

    public Gate(GateServer gateServer) {
        this.gateId = counter++;
        this.isOpen = true;
        this.gateServer = gateServer;
    }

    public int getGateId() {
        return gateId;
    }

    public Optional<Ticket> generateTicket(String registrationNumber) {
        if (gateServer.getGatesToParking().get(this.gateId).hasFreeSpot()) {
            Ticket ticket = gateServer.generateTicket(registrationNumber);
            gateServer.getRegIdGateMap().put(this.gateId, registrationNumber);
            return Optional.of(ticket);
        }
        System.out.println("Parking pełny, podążaj za białym królikiem na inny ;)");
        return Optional.empty();
    }

    public boolean checkIn(String registrationNumber) {
        if (!isOpen) {
            return false;
        }
        Optional<Ticket> generated = generateTicket(registrationNumber);

        if (generated.isPresent()) {
            System.out.println("Wygenerewoano ticket: " + generated.get());
            return true;
        }

        return false;

    }

    public boolean checkOut(int ticketID,String registrationNumber) {

//        if()

        return false;
    }

}



